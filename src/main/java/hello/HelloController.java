package hello;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

@RestController
public class HelloController {
    Log logger = LogFactory.getLog(HelloController.class);
    int mCount=0;

    @RequestMapping(method= RequestMethod.GET)
    public String get(HttpServletRequest request) {
        logger.error(String.format("[%d] GET %s, from %s:%s", mCount++, request.getRequestURI(), request.getRemoteHost(), request.getRemotePort()));

        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            logger.error(headerName + ":" + request.getHeader(headerName));
        }

        Enumeration params = request.getParameterNames();
        while(params.hasMoreElements()){
            String paramName = (String)params.nextElement();
            logger.error(paramName+ ":" + request.getParameter(paramName));
        }

        logger.error("");

        // 重定向到优易付
        // 返回response
        return "get data";
    }

    @RequestMapping(method= RequestMethod.POST)
    public String post(HttpServletRequest request) {
        logger.error(String.format("[%d] POST %s, from %s:%s", mCount++, request.getRequestURI(), request.getRemoteHost(), request.getRemotePort()));

        Enumeration headerNames = request.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            logger.error(headerName + ":" + request.getHeader(headerName));
        }

        Enumeration params = request.getParameterNames();
        while(params.hasMoreElements()){
            String paramName = (String)params.nextElement();
            logger.error(paramName + ":" + request.getParameter(paramName));
        }

        BufferedReader br = null;
        try {
            br = request.getReader();
            String str, wholeStr = "";
            while((str = br.readLine()) != null){
                wholeStr += str;
            }
            logger.error(wholeStr);
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.error("");

        // 重定向到优易付
        // 返回response
        return "post data";
    }


    
}
